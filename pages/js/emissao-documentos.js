$(document).ready(function() {
	var students = [
		{id: 1, name: "Lucas de Holanda"},
		{id: 2, name: "Rodrigo Monteiro"},
		{id: 3, name: "Douglas Sanatana"},
		{id: 4, name: "Alan Kléber"}
	];

	$("#input-aluno-comprovante, #input-aluno-conclusao").typeahead({
		source: students,
		minLength: 0
	});

	$("#emission-date-comprovante, #emission-date-conclusao").datepicker({
		clearBtn: true,
		language: "pt-BR",
		showOnFocus: false,
		todayHighlight: true,
		todayBtn: true
	});
});
